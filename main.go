package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

var (
	prefix    string
	digits    int
	extension string
)

func getNumber(num int, digits int) string {
	format := "%" + fmt.Sprintf("0%dd", digits)
	return fmt.Sprintf(format, num)
}

func init() {
	flag.StringVar(&prefix, "p", "file_", "Prefix of files")
	flag.IntVar(&digits, "d", 3, "Number of digits")
	flag.StringVar(&extension, "e", "txt", "Ext")

	flag.Parse()
}

func main() {
	var (
		dir   string
		files []os.FileInfo
		err   error
	)

	if dir, err = os.Getwd(); err != nil {
		log.Fatal(err)
	}

	if files, err = ioutil.ReadDir(dir); err != nil {
		log.Fatal(err)
	}

	counter := 1
	for _, file := range files {
		path := fmt.Sprintf("%s/%s", dir, file.Name())
		newPath := fmt.Sprintf("%s/%s%s.%s", dir, prefix, getNumber(counter, digits), extension)
		os.Rename(path, newPath)
		counter++
	}
}
