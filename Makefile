PROJECT_ROOT := $(shell pwd)
PROJECT := $(notdir $(shell pwd))

.ONESHELL:


# Service
clean-project:
	@ echo "Clear project [${PROJECT}]"
	@ rm -f build/app

# Build app
build-app: clean-project
	@ echo "Build app [${PROJECT}]"
	@ go get
	@ CGO_ENABLED=0 go build -i -o ./build/renamer ./*.go
